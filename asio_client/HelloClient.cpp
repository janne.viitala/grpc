#include "HelloClient.h"


HelloClient::AsyncClientCall::~AsyncClientCall ()
{
}


HelloClient::HelloClient(shared_ptr<grpc::Channel> channel, boost::asio::io_service& ioservice)
    : boost::asio::io_service::service (ioservice),
      m_stub (Greeter::NewStub (channel)),
      m_ioservice (ioservice)
{
    std::cout << "hello_service created\n";
    m_thread = std::thread (&HelloClient::messagePump, this);
}


void HelloClient::shutdown_service()
{
    std::cout << "shutting down HelloClient (as soon as handler thread exits...)" << std::endl;
    m_thread.join ();
}


std::string HelloClient::sayHello (const std::string &name)
{
    HelloRequest req;
    HelloReply repl;
    grpc::ClientContext context;
    req.set_name (name);
    grpc::Status s = m_stub->SayHello (&context, req, &repl);
    if (s.ok())
    {
        return repl.message();
    }
    else
    {
        std::cout << "voivoi!\n";
        return "";
    }
}


void HelloClient::startSayHello (const string& name, function<void (string)> cb)
{
    HelloRequest req;
    AsyncHelloCall *call = new AsyncHelloCall;
    req.set_name (name);
    call->cb = cb;
    call->response_reader = m_stub->AsyncSayHello (&call->context, req, &m_cq);
    call->response_reader->Finish (&call->reply, &call->status, reinterpret_cast<void*>(call));
    std::cout << "posted async rq" << std::endl;
}


double HelloClient::summaa (double a, double b)
{
    Summattavat req;
    SummaTulos resp;
    req.set_a (a);
    req.set_b (b);
    grpc::ClientContext context;
    grpc::Status s = m_stub->Summaa (&context, req, &resp);
    if (s.ok ())
        return resp.result ();
    else
    {
        std::cout << "voivoi error" <<std::endl;
        return 0;
    }
}


void HelloClient::startSummaa (double a, double b, std::function<void (double)> cb)
{
    Summattavat req;
    AsyncSummaaCall *call = new AsyncSummaaCall;
    req.set_a (a);
    req.set_b (b);
    call->cb = cb;
    call->response_reader = m_stub->AsyncSummaa (&call->context, req, &m_cq);
    call->response_reader->Finish (&call->reply, &call->status, reinterpret_cast<void*>(call));
    std::cout << "posted async rq" << std::endl;
}


void HelloClient::messagePump ()
{
    usleep (5000);
    void* got_tag;
    bool ok = false;
    while (1)
    {
        std::cout << "thread waiting msg!" << std::endl;
        GPR_ASSERT (m_cq.Next (&got_tag, &ok));
        std::cout << "thread got msg!!" << std::endl;
        if (ok)
        {
            AsyncHelloCall *hc;
            AsyncSummaaCall *sc;
            AsyncClientCall *call = static_cast<AsyncClientCall*>(got_tag);
            switch (call->id ())
            {
            case HELLO:
                hc = static_cast<AsyncHelloCall*> (call);
                std::cout << "posting callback to asio ioservice, currently in thread  " << tid () << std::endl;
                m_ioservice.post (bind(hc->cb, hc->reply.message ()));
                break;
            case SUMMAA:
                sc = static_cast<AsyncSummaaCall*> (call);
                sc->cb (sc->reply.result ());
                m_ioservice.post (bind(sc->cb, sc->reply.result ()));
                break;
            }
            delete call;
        }
    }
    std::cout << "thread end!" << std::endl;
}
