#include <boost/asio/io_service.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <chrono>
#include <iostream>
#include <pthread.h>

#include "HelloClient.h"

using namespace boost;
using namespace boost::asio;

static deadline_timer *tmr;


unsigned short tid ()
{
    return static_cast<unsigned short> (pthread_self ());
}


// some other event based activity here
void on_timer (const boost::system::error_code& /*e*/)
{
    cout << "kello! (thread=" << tid () << ")\n";
    tmr->expires_at (tmr->expires_at () + posix_time::milliseconds (3000));
    tmr->async_wait (&on_timer);  // this keeps asio event loop alive (it exists if it has nothing to do)
}


int main ()
{
    io_service ioservice;
    auto chn = grpc::CreateChannel ("127.0.0.1:50051", grpc::InsecureChannelCredentials ());
    HelloClient hc (chn, ioservice);

    auto waitTime = gpr_time_add (gpr_now (GPR_CLOCK_REALTIME), gpr_time_from_seconds (5, GPR_TIMESPAN));
    if (chn->WaitForConnected (waitTime))
    {
        // example asio asynchronous object - timer
        tmr = new deadline_timer (ioservice);
        tmr->expires_from_now (posix_time::milliseconds (300));
        tmr->async_wait (&on_timer);

        // grpc call(s)
        hc.startSayHello (string ("rane"), [&hc](string s) {
            cout << "ASYNC VALMIS: " << s << " (thread=" << tid () << ")" << endl;
            cout << "summa on " << hc.summaa (12, 34) << endl;
        });

        cout << "enter ioservice event loop in thread " << tid () << "\n";
        ioservice.run ();
    }
}
