/* HelloClient
 *
 * Using boost::asio to provide event loop
 *
 */

#pragma once

#include <boost/asio.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>
#include <chrono>
#include <thread>
#include <functional>

#include <grpcpp/grpcpp.h>
#include "Hello.grpc.pb.h"


extern unsigned short tid ();

using namespace std;

class HelloClient: public boost::asio::io_service::service
{
public:
    HelloClient (shared_ptr<grpc::Channel> channel, boost::asio::io_service &ioservice);

    string sayHello (const string &name);
    void startSayHello (const string &name, function<void (string)> cb);

    double summaa (double a, double b);
    void startSummaa (double a, double b, function<void (double result)> cb);

private:
    void shutdown_service ();
    void messagePump ();

    enum ID
    {
        HELLO,
        SUMMAA
    };
    struct AsyncClientCall          // struct for keeping state and data information over the async call
    {
        virtual ~AsyncClientCall ();
        virtual int id () = 0;
        grpc::ClientContext context;
        grpc::Status status;
    };
    struct AsyncHelloCall : public AsyncClientCall
    {
        int id () { return HELLO; }
        HelloReply reply;
        function<void (string msg)> cb;
        unique_ptr<grpc::ClientAsyncResponseReader<HelloReply>> response_reader;
    };
    struct AsyncSummaaCall : public AsyncClientCall
    {
        int id () { return SUMMAA; }
        SummaTulos reply;
        function<void (double result)> cb;
        unique_ptr<grpc::ClientAsyncResponseReader<SummaTulos>> response_reader;
    };

    unique_ptr<Greeter::Stub>  m_stub;  // generated grpc Stub
    grpc::CompletionQueue      m_cq;
    boost::asio::io_service&   m_ioservice;
    thread                     m_thread;
};

