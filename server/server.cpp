
#include "greeterserviceimpl.h"
#include "testiserviceimpl.h"
#include "kanavaserviceimpl.h"

#include "grpcservicerunner.h"

#include <unistd.h>  // sleep



int main ()
{
    std::string serverAddress ("0.0.0.0:50051");
    std::string serverAddress2 ("0.0.0.0:50052");
    std::string serverAddress3 ("0.0.0.0:50053");
    GRPCServiceRunner<GreeterServiceImpl>  s (serverAddress);
    GRPCServiceRunner<TestiServiceImpl>   s2 (serverAddress2);
    GRPCServiceRunner<TstChannelServiceImpl>  s3 (serverAddress3);

    s2.runThreaded ();
    s3.runThreaded ();

    std::cout << "Server listening on " << serverAddress << " and " << serverAddress2 << "" << std::endl;
    s.runThreaded (ServiceThreadMode_Joinable);
    for (auto i: {1,2,3,4,5,6,7,8,9,10})
    {
        sleep (1);
        std::cout << i << "..." << std::endl;
    }
    std::cout << "Joining thread1..." << std::endl;
    s.joinThread ();
    return 0;
}

