#pragma once

#include "TstChannel.grpc.pb.h"

class TstChannelServiceImpl final : public TstChannel::Service
{
public:
    grpc::Status connect (grpc::ServerContext *context, ::grpc::ServerReaderWriter<ConnectReply, ConnectRequest> *stream);
};

