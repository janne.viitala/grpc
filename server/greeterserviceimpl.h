#ifndef GREETERSERVICEIMPL_H
#define GREETERSERVICEIMPL_H

#include "Hello.grpc.pb.h"


class GreeterServiceImpl final : public Greeter::Service
{
public:
    // Handlers for IDL functions
    // Note: may come in any thread context!
    grpc::Status SayHello (grpc::ServerContext* context, const HelloRequest* request, HelloReply* reply) override;
    grpc::Status Summaa (grpc::ServerContext* context, const Summattavat* request, SummaTulos* response) override;
};


#endif // GREETERSERVICEIMPL_H
