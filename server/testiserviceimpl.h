#ifndef TESTISERVICEIMPL_H
#define TESTISERVICEIMPL_H

#include "Testi.grpc.pb.h"

class TestiServiceImpl final : public Testi::Service
{
public:
    // Service interface
    grpc::Status doStuff (grpc::ServerContext* context, const TestiRequest* request, TestiReply* response);
};

#endif // TESTISERVICEIMPL_H
