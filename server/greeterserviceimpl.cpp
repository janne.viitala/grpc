#include "greeterserviceimpl.h"

#include <iostream>


unsigned short tid () { return pthread_self () & 0xffff; }


grpc::Status GreeterServiceImpl::SayHello (grpc::ServerContext*, const HelloRequest* request, HelloReply* reply)
{
    static int cnt;
    std::cout << "got call " << cnt << " from " << request->name() << ", thread " << tid () << std::endl;
    reply->set_message (std::string ("pöö ") + std::to_string (cnt++) + " " + request->name());
    return grpc::Status::OK;
}


grpc::Status GreeterServiceImpl::Summaa (grpc::ServerContext*, const Summattavat* request, SummaTulos* response)
{
    response->set_result(request->a () + request->b ());
    return grpc::Status::OK;
}
