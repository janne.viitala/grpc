#include "testiserviceimpl.h"

#include <iostream>

grpc::Status TestiServiceImpl::doStuff (grpc::ServerContext*, const TestiRequest* request, TestiReply* response)
{
    std::cout << "command is " << request->command ().command () << std::endl;
    for (int i=0; i<request->names_size(); i++)
        std::cout << request->names (i) << std::endl;
    response->set_mode (TestiReply_Mode_IDLE);
    return grpc::Status::OK;
}
