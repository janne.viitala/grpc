/*  GRPCServiceRunner
 *
 *  Simple template class to run gRPC services synchronousely or
 *  in own thread.
 */

#ifndef GRPCSERVICERUNNER_H
#define GRPCSERVICERUNNER_H

#include <grpc++/server.h>
#include <grpc++/server_builder.h>
#include <grpc++/security/server_credentials.h>

#include <thread>

enum ServiceThreadMode { ServiceThreadMode_Detached, ServiceThreadMode_Joinable };

template <class ServiceType> class GRPCServiceRunner
{
public:
    GRPCServiceRunner (const std::string& addr)  // e.g. "0.0.0.0:12345"
    {
        m_builder.AddListeningPort (addr, grpc::InsecureServerCredentials ());
    }

    // Blocking
    void run ()
    {
        m_builder.RegisterService (&m_service);
        std::unique_ptr<grpc::Server> server (m_builder.BuildAndStart ());
        server->Wait ();
    }

    // Run in background thread, either detached or joinable mode (like std::thread)
    void runThreaded (ServiceThreadMode m = ServiceThreadMode_Detached)
    {
        m_thread = std::thread (&GRPCServiceRunner::run, this);
        if (m == ServiceThreadMode_Detached)
            m_thread.detach ();
    }

    // Join thread, if started in Joinable mode
    void joinThread ()
    {
        if (m_thread.joinable ())
        {
            m_thread.join ();
        }
        else
        {
            std::cout << "Error: GRPCServer::joinThread(): service thread is detached, can not join" << std::endl;
        }
    }

private:
    grpc::ServerBuilder m_builder;
    ServiceType         m_service;
    std::thread         m_thread;
};

#endif // GRPCSERVICERUNNER_H
