#ifndef HELLOCLIENT_H
#define HELLOCLIENT_H


#include <grpcpp/grpcpp.h>

#include "Hello.grpc.pb.h"
#include <memory>  // unique_ptr
#include <string>
#include <functional>
#include <thread>

class HelloClient
{
public:
    HelloClient (std::shared_ptr<grpc::Channel> channel);  // necessary constructor
    ~HelloClient ();

    // Wrappers for functions in IDL
    // These are needed as e.g. string may be const char*, std::string, QString, wxString, etc.,
    // containers too may be different and so on.
    // Also, error handling may differ from case to case.
    std::string sayHello (const std::string &name);
    void startSayHello (const std::string &name, std::function<void (std::string)> cb); // , std::function< std::string () > cb);

    double summaa (double a, double b);
    void startSummaa (double a, double b, std::function<void (double result)> cb);


private:
    // async call handling - we want to provide nice callbacks so we need some infra on top of CompletionQueue
    void messagePump ();
    enum ID
    {
        HELLO,
        SUMMAA
    };
    struct AsyncClientCall          // struct for keeping state and data information over the async call
    {
        virtual ~AsyncClientCall ();
        virtual int id () = 0;
        grpc::ClientContext context;
        grpc::Status status;
    };
    struct AsyncHelloCall : public AsyncClientCall
    {
        int id () { return HELLO; }
        HelloReply reply;
        std::function<void (std::string msg)> cb;
        std::unique_ptr<grpc::ClientAsyncResponseReader<HelloReply>> response_reader;
    };
    struct AsyncSummaaCall : public AsyncClientCall
    {
        int id () { return SUMMAA; }
        SummaTulos reply;
        std::function<void (double result)> cb;
        std::unique_ptr<grpc::ClientAsyncResponseReader<SummaTulos>> response_reader;
    };

    std::unique_ptr<Greeter::Stub> m_stub;  // generated Stub
    grpc::CompletionQueue m_cq;
    std::thread m_thr;
};


#endif // HELLOCLIENT_H
