#include "client.h"

#include "helloclient.h"
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

#include <stdio.h>
#include <memory>



const char *channelState2Str (grpc_connectivity_state s)
{
#define CASE(tag) case tag: return #tag
    switch (s)
    {
    CASE(GRPC_CHANNEL_IDLE);
    CASE(GRPC_CHANNEL_CONNECTING);
    CASE(GRPC_CHANNEL_READY);
    CASE(GRPC_CHANNEL_TRANSIENT_FAILURE);
    CASE(GRPC_CHANNEL_SHUTDOWN);
    }
    return "bugbug";
#undef CASE
}


int main ()
{
    printf ("create channel...\n");
    auto chn = grpc::CreateChannel ("127.0.0.1:50051", grpc::InsecureChannelCredentials ());
    printf ("figure out time...\n");
    auto t = gpr_time_add (
                 gpr_now (GPR_CLOCK_REALTIME),
                 gpr_time_from_seconds (600, GPR_TIMESPAN));
    printf ("tila on %s\n", channelState2Str (chn->GetState (true)));
    printf ("wait for connection to the server...\n");
    if (chn->WaitForConnected (t))
    {
        HelloClient c (chn);
        std::cout << "'" << c.sayHello ("putte").c_str() << "'" << std::endl;   // synchronous
        std::cout << "summa on " << c.summaa (12, 34) << std::endl;             // synchronous
        // some async calls
        c.startSayHello ("asyncpossu", [](std::string s) {
            std::cout << "ASYNC VALMIS: " << s << std::endl;
        });
        for (int i=0; i<10; i++)
        {
            c.startSummaa (99, 77+i, [](double result) {
                std::cout << "ASYNC summaus VALMIS: " << result << std::endl;
            });
            // observation: callback may come in different order than requests were sent
        }
    }
    else
    {
        printf ("ei yhteyttä, harmi\n");
    }
    return 0;
}

