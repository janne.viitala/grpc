KOE
===

apt install libprotobuf-dev
apt install libgrpc++-dev
apt install protobuf-compiler
BUT THIS DOES NOT WORK ON Ubuntu 18/04, install grpc from sources!!


https://bugs.launchpad.net/ubuntu/+source/grpc/+bug/1797000


~/src/grpc_koe/client[master]$ ldd client
	linux-vdso.so.1 (0x00007ffe7cbb6000)
	libgrpc++.so.1 => /usr/local/lib/libgrpc++.so.1 (0x00007f2d45b8d000)
	libgrpc.so.7 => /usr/local/lib/libgrpc.so.7 (0x00007f2d45800000)
	libgrpc++_reflection.so.1 => /usr/local/lib/libgrpc++_reflection.so.1 (0x00007f2d45414000)
	libstdc++.so.6 => /usr/lib/x86_64-linux-gnu/libstdc++.so.6 (0x00007f2d4508b000)
	libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f2d44e73000)
	libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f2d44c54000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f2d44863000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f2d461f0000)
	libssl.so.1.0.0 => /usr/lib/x86_64-linux-gnu/libssl.so.1.0.0 (0x00007f2d445fb000)
	libcrypto.so.1.0.0 => /usr/lib/x86_64-linux-gnu/libcrypto.so.1.0.0 (0x00007f2d441b8000)
	librt.so.1 => /lib/x86_64-linux-gnu/librt.so.1 (0x00007f2d43fb0000)
	libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f2d43c12000)
	libz.so.1 => /lib/x86_64-linux-gnu/libz.so.1 (0x00007f2d439f5000)
	libcares.so.2 => /usr/lib/x86_64-linux-gnu/libcares.so.2 (0x00007f2d437e3000)
	libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f2d435df000)
~/src/grpc_koe/client[master]$ 

