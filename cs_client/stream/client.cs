using System;
using System.Threading.Tasks;
using Grpc.Core;

namespace stream_client
{
    class TstChannelClient
    {
        public async void HandleConnect(TstChannel.TstChannelClient cl)
        {
            using (var call = cl.connect())
            {
                var responseReaderTask = Task.Run(async () =>
                {
                    while (await call.ResponseStream.MoveNext())
                    {
                        var note = call.ResponseStream.Current;
                        Console.WriteLine("Received " + note);
                    }
                });

                var req = new ConnectRequest { Name = "ss", Address = "1.23." };
                for (var i=0; i<10; i++)
                {
                    await call.RequestStream.WriteAsync(req);
                }
                await call.RequestStream.CompleteAsync();
                await responseReaderTask;
            }
            
        }
    }
}