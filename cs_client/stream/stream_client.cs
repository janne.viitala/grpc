using System;
using Grpc.Core;

namespace stream_client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(DateTime.Now.ToString("hh:mm.ss.ffffff") + " main thread: " + System.Threading.Thread.CurrentThread.ManagedThreadId);
                        
            var channel = new Channel("localhost:50053", ChannelCredentials.Insecure);
            var tstch = new TstChannel.TstChannelClient(channel);
            var client = new TstChannelClient();
            client.HandleConnect(tstch);
            System.Threading.Thread.Sleep(2000);
        }
    }
}
