using System;
using Grpc.Core;

namespace koe_client
{
  class Program
  {
    static void Main(string[] args)
    {
      var channel = new Channel("localhost:50051", ChannelCredentials.Insecure);
      var greeter = new Greeter.GreeterClient(channel);

      var reply_async = greeter.SayHelloAsync(new HelloRequest { Name = "rane" });  // async call
      var reply_sync = greeter.SayHello(new HelloRequest { Name = "puttebossu" });  // sync call
      Console.WriteLine("vastays=" + reply_sync.Message);
      // async gprc call returns object that has two Task's, one for headers and other for response:
      reply_async.ResponseAsync.Wait();                                             // wait completion
      Console.WriteLine("async response: " + reply_async.ResponseAsync.Result.Message);
    }
  }
}
