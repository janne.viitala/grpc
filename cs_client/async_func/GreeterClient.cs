/*  GreeterClient - grpc wrapper
 *
 */

using System;
using Grpc.Core;
using System.Threading.Tasks;

class GreeterClient
{
  Channel m_channel;
  Greeter.GreeterClient m_greeter;

  public GreeterClient()
  {
    m_channel = new Channel("localhost:50051", ChannelCredentials.Insecure);
    m_greeter = new Greeter.GreeterClient(m_channel);
  }

  public async Task<string> SayHelloAsync (string name)
  {
    var r = await m_greeter.SayHelloAsync(new HelloRequest { Name = name });
    return r.Message;
  }

  // callback based asynchronous function, not recommended in .netcore3
  public async void StartSayHello(string name, Action<string> cb)
  {
    Console.WriteLine(DateTime.Now.ToString("hh:mm.ss.ffffff") + " thread calling wrapper SayHelloAsync: " + System.Threading.Thread.CurrentThread.ManagedThreadId);
    var r = m_greeter.SayHelloAsync(new HelloRequest { Name = name });
    /*  
     * NOTE: 
     *  in GUI C# application there is "SynchronizationContext", "that schedules 
     *  continuations back onto the UI thread". But in non-GUI apps threads work
     *  like in any non-eventloop app, i.e. mainthread is doing something elsewhere
     *  and .net runtime pick new threadpool thread to run rest of this async function.
     *  So callback is called in context of some random thread.
     * 
     *  https://stackoverflow.com/questions/33821679/async-await-different-thread-id
     *  https://docs.microsoft.com/en-us/archive/msdn-magazine/2011/february/msdn-magazine-parallel-computing-it-s-all-about-the-synchronizationcontext
     *
     */
    await r.ResponseAsync;
    Console.WriteLine(DateTime.Now.ToString("hh:mm.ss.ffffff") + " thread after wrapper SayHelloAsync: " + System.Threading.Thread.CurrentThread.ManagedThreadId);
    cb(r.ResponseAsync.Result.Message);
  }

  // sync version
  public string SayHello(string name)
  {
    var r = m_greeter.SayHello(new HelloRequest { Name = name });
    return r.Message;
  }


}