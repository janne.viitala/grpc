using System;
using System.Threading.Tasks;

namespace koe_client
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine(DateTime.Now.ToString("hh:mm.ss.ffffff") + " main thread: " + System.Threading.Thread.CurrentThread.ManagedThreadId);
      var c = new GreeterClient();  // grpc wrapper
      int i = 0;

      /* 
       *  Callback based asynchronous function.
       *  This callback is called in parallel thread context.
       *  Note: not recommended way with .net nowasays
       */
      c.StartSayHello("reiska", (r) => {  
        i = 0;
        System.Console.WriteLine(DateTime.Now.ToString("hh:mm.ss.ffffff") + " callback in main() received: " + r);
      });
      // do something while we wait async function to complete
      const int LOOPS = 10;
      for (int j = 0; j < LOOPS; j++)
      {
        System.Console.WriteLine(DateTime.Now.ToString("hh:mm.ss.ffffff") + " loop " + j + " and i=" + i++);
      }
      if (i != LOOPS)  // we have a corrupton here - but not in similar GUI app(!)
        System.Console.WriteLine(DateTime.Now.ToString("hh:mm.ss.ffffff") + " Data corruption, " + i + " != " + LOOPS + "!!");

      /* 
       *  Task based asynchronous function.
       *  Thread safe, but requires polling in .net core, as there is no event loop       
       */
      var t = c.SayHelloAsync("rane");
      // do something while we wait async function to complete
      for (int j = 0; j < LOOPS; j++)
      {
        i++;
        System.Console.WriteLine(DateTime.Now.ToString("hh:mm.ss.ffffff") + " loop " + j + " and i=" + i);
      }
      if (t.IsCompleted)
        System.Console.WriteLine(DateTime.Now.ToString("hh:mm.ss.ffffff") + " async operation is complete: " + t.Result);

      System.Console.WriteLine(DateTime.Now.ToString("hh:mm.ss.ffffff") + " main is ready, bye");
    }
  }
}
